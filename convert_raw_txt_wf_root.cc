//
#include <TH1D.h>
#include <TStyle.h>
#include <TString.h>
#include <TCanvas.h>
#include <TFile.h>
#include <TTree.h>
#include <TGraph.h>

//C, C++
#include <stdio.h>
#include <assert.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <string>
#include <iomanip>
#include <sstream>
#include <vector>

using namespace std;

//////////////////////////////////////////
//LECROYWaveRunner<F9><9E>        40697   Waveform
//Segments        1000    SegmentSize     50002
//Segment TrigTime        TimeSinceSegment1
//#1      03-Nov-2023 11:06:55    0                 
//#2      03-Nov-2023 11:06:55    2.28429e-05       
//#3      03-Nov-2023 11:06:55    4.45388e-05       
//#4      03-Nov-2023 11:06:55    6.61846e-05       
//#5      03-Nov-2023 11:06:55    8.85924e-05       
//...
//#1000   03-Nov-2023 11:06:55    0.0221659      
//Time    Ampl
//-1.0000061e-05  -0.0128515
//-9.9996613e-06  -0.0128515
//-9.9992613e-06  -0.0128515
//-9.9988613e-06  -0.0128515
//////////////////////////////////////////

const Int_t nnPoints = 1000000;
const Int_t nMaxWf = 100000;
const Float_t load_R = 50; //ohm
const Float_t baseline_window_in_ns = 40;//ns

void convtoroot(TString txtFile, TString rootFile,
		Double_t charge_min, Double_t charge_max,
		Double_t baseline_ampl_min, Double_t baseline_ampl_max);
void get_data_length(TString txtFile, Int_t *segmentSize_arr, Int_t &nWf);
Float_t get_total_charge(TGraph *gr, Float_t R);
void get_baseline_info( TGraph *gr, Float_t R, Float_t baseline_window_in_ns, Float_t &baseline_ampl, Float_t &baseline_charge);
Float_t get_max_ampl( TGraph *gr, Float_t &max_ampl_time);
void shift_and_norm_gr( TGraph *gr, TGraph *gr_norm);
void shift_and_norm_gr( TGraph *gr, TGraph *gr_norm, Bool_t if_norm_max);
void save_gr_file( TGraph *gr, TString file_name);

int main(int argc, char *argv[]){
  if(argc == 7 && atoi(argv[1]) == 0){
    TString txtFile = argv[2];
    Double_t charge_min = atof(argv[3]);
    Double_t charge_max = atof(argv[4]);
    Double_t baseline_ampl_min = atof(argv[5]);
    Double_t baseline_ampl_max = atof(argv[6]);
    cout<<"txtFile           : "<<txtFile<<endl
	<<"charge_min        : "<<charge_min<<endl
	<<"charge_max        : "<<charge_max<<endl
	<<"baseline_ampl_min : "<<baseline_ampl_min<<endl
	<<"baseline_ampl_max : "<<baseline_ampl_max<<endl;
    TString rootFile = txtFile + ".root";
    convtoroot( txtFile, rootFile,
		charge_min, charge_max,
		baseline_ampl_min, baseline_ampl_max);
  }
  else{
    cout<<"  ERROR ---> in input arguments "<<endl
	<<"             [1] : 0"<<endl
	<<"             [2] : file to convert"<<endl
      	<<"             [3] : charge min"<<endl
	<<"             [4] : charge max"<<endl
	<<"             [5] : baseline ampl. min"<<endl
	<<"             [6] : baseline ampl. max"<<endl;
  }
  return 0;
}

void get_data_length(TString txtFile, Int_t *segmentSize_arr, Int_t &nWf){
  string mot;
  Int_t nSegments;
  Int_t nSegmentSize;
  nWf = 0;
  for(Int_t i = 0;i<nMaxWf;i++)
    segmentSize_arr[i] = 0;
  ifstream indata;
  indata.open(txtFile.Data()); 
  assert(indata.is_open());
  indata>>mot>>mot>>mot>>mot
	>>nSegments
	>>mot
	>>nSegmentSize
	>>mot>>mot>>mot;
  //cout<<"nSegments     : "<<nSegments<<endl
  //  <<"nSegmentSize  : "<<nSegmentSize<<endl;
  if(nnPoints<nSegmentSize){
    cout<<" ERROR ---> nnPoints     < nSegmentSize "<<endl
	<<"            nnPoints     = "<<nnPoints<<endl
	<<"            nSegmentSize = "<<nSegmentSize<<endl;
    assert(0);
  }
  for(Int_t i = 0; i<nSegments; i++)
    indata>>mot>>mot>>mot>>mot;
  indata>>mot>>mot;
  //
  Float_t t_val;
  Float_t t_val_old;
  Float_t Volt_val;
  //
  Int_t nTot = 0;
  //
  Int_t n_wf_points = 0;
  //
  indata>>t_val_old>>Volt_val;
  nTot++;
  n_wf_points++;
  while(indata>>t_val>>Volt_val){
    //cout<<t_val<<" "<<Volt_val<<endl;
    if((t_val - t_val_old)>0){
      n_wf_points++;
    }
    else{
      segmentSize_arr[nWf] = n_wf_points;
      //cout<<nWf<<" "<<n_wf_points<<endl;
      n_wf_points = 0;
      n_wf_points++;
      nWf++;
    }
    t_val_old = t_val;
    nTot++;
  }
  segmentSize_arr[nWf] = n_wf_points;
  nWf++;
  indata.close();
}

void convtoroot(TString txtFile, TString rootFile,
		Double_t charge_min, Double_t charge_max,
		Double_t baseline_ampl_min, Double_t baseline_ampl_max){
  //  
  Int_t nWf = 0;
  //
  Int_t *segmentSize_arr = new Int_t[nMaxWf];
  get_data_length(txtFile, segmentSize_arr, nWf);
  //for(Int_t i = 0;i<nWf;i++)
  //cout<<i<<" "<<segmentSize_arr[i]<<endl;
  //assert(0);
  //
  Int_t waveformID = 0;
  Int_t NPoints;
  Float_t tot_charge;
  Float_t baseline_ampl;
  Float_t baseline_charge;
  Float_t Time[nnPoints];
  Float_t Volt[nnPoints];
  //
  Double_t t_ave, a, a_ave;
  //
  vector<TGraph*> v_gr;
  vector<TGraph*> v_gr_cut;
  TH1D *h1_base_line_ampl = new TH1D("h1_base_line_ampl","h1_base_line_ampl",4000,-0.2,0.2);
  TH1D *h1_base_line_ampl_cut = new TH1D("h1_base_line_ampl_cut","h1_base_line_ampl_cut",4000,-0.2,0.2);
  TH1D *h1_tot_charge = new TH1D("h1_tot_charge","h1_tot_charge",4000,-2.0*1.0e-9,2.0*1.0e-9);
  TH1D *h1_tot_charge_cut = new TH1D("h1_tot_charge_cut","h1_tot_charge_cut",4000,-2.0*1.0e-9,2.0*1.0e-9);
  //
  TGraph *gr_ave = new TGraph();
  gr_ave->SetNameTitle("gr_ave","gr_ave");
  TGraph *gr_ave_norm = new TGraph();
  gr_ave_norm->SetNameTitle("gr_ave_norm","gr_ave_norm");
  TGraph *gr_ave_shift = new TGraph();
  gr_ave_shift->SetNameTitle("gr_ave_shift","gr_ave_shift");
  TGraph *gr_rnd_norm = new TGraph();
  gr_rnd_norm->SetNameTitle("gr_rnd_norm","gr_rnd_norm");
  //
  ///////////////////Root file with data/////////////////
  TFile *hfile = new TFile( rootFile, "RECREATE", "LeCroy Data", 1);
  if (hfile->IsZombie()) {
    cout<<" ---> ERROR : PROBLEM with the initialization of the output ROOT file : "<<endl 
        <<rootFile
        <<endl;
    assert(0);
  }
  TTree *tree = new TTree("T", "WF data");
  hfile->SetCompressionLevel(2);
  tree->SetAutoSave(1000000);  
  // Create new event
  TTree::SetBranchStyle(0);
  ///////////////////////////////////////////////////////
  //  
  //Event////////////////////////////////////////////////
  tree->Branch("waveformID",&waveformID, "waveformID/I");
  tree->Branch("NPoints",&NPoints, "NPoints/I");
  tree->Branch("tot_charge", &tot_charge, "tot_charge/F");
  tree->Branch("baseline_ampl", &baseline_ampl, "baseline_ampl/F");
  tree->Branch("baseline_charge", &baseline_charge, "baseline_charge/F"); 
  tree->Branch("Time", Time, "Time[NPoints]/F"); 
  tree->Branch("Volt", Volt, "Volt[NPoints]/F"); 
  ///////////////////////////////////////////////////////
  //
  string mot;
  Float_t Volt_val;
  //
  Float_t t_val = 0.0;
  //
  Int_t nSegments;
  Int_t nSegmentSize;
  //  
  ifstream indata;
  indata.open(txtFile.Data()); 
  assert(indata.is_open());
  cout<<" ---> Conversion of "<<txtFile<<endl;
  //
  //LECROYWR620Zi 62339 Waveform
  //Segments 1000 SegmentSize 5002
  //Segment TrigTime TimeSinceSegment1
  indata>>mot>>mot>>mot>>mot
	>>nSegments
	>>mot
	>>nSegmentSize
	>>mot>>mot>>mot;
  cout<<"nSegments     : "<<nSegments<<endl
      <<"nSegmentSize  : "<<nSegmentSize<<endl;
  if(nnPoints<nSegmentSize){
    cout<<" ERROR ---> nnPoints     < nSegmentSize "<<endl
	<<"            nnPoints     = "<<nnPoints<<endl
	<<"            nSegmentSize = "<<nSegmentSize<<endl;
    assert(0);
  }
  for(Int_t i = 0; i<nSegments; i++)
    indata>>mot>>mot>>mot>>mot;
  indata>>mot>>mot;
  //
  for(Int_t i = 0; i<nWf; i++){
    TGraph *gr = new TGraph();
    TString gr_nt = "gr_";
    gr_nt += waveformID;
    gr->SetNameTitle(gr_nt.Data(),gr_nt.Data());
    for(Int_t j = 0;j<segmentSize_arr[i]; j++){
      indata>>t_val>>Volt_val;
      Volt[j] = Volt_val;
      Time[j] = t_val;
      gr->SetPoint(j,t_val,Volt_val);
    }
    v_gr.push_back(gr);
    //v_h1_base_line_ampl.push_back(h1_base_line_ampl);
    tot_charge=get_total_charge(gr,load_R);
    h1_tot_charge->Fill(tot_charge);
    get_baseline_info( gr, load_R, baseline_window_in_ns, baseline_ampl, baseline_charge);
    h1_base_line_ampl->Fill(baseline_ampl);
    if( (tot_charge > charge_min) && (tot_charge < charge_max) &&
	(baseline_ampl > baseline_ampl_min) && (baseline_ampl < baseline_ampl_max) ){
      v_gr_cut.push_back(gr);
      h1_base_line_ampl_cut->Fill(baseline_ampl);
      h1_tot_charge_cut->Fill(tot_charge);
    }
    NPoints = segmentSize_arr[i];
    tree->Fill();
    waveformID++;
  }
  indata.close();
  hfile = tree->GetCurrentFile();
  hfile->Write();
  hfile->Close();
  //

  //
  TString wf_csv_file_name;
  wf_csv_file_name = txtFile; wf_csv_file_name += "_norm.dat"; 
  save_gr_file( gr_ave_norm, wf_csv_file_name);
  wf_csv_file_name = txtFile; wf_csv_file_name += "_shift.dat"; 
  save_gr_file( gr_ave_shift, wf_csv_file_name);
  ///////////////
  TString nameF = rootFile;
  nameF += "_gr.root";
  TFile* gr_rootFile = new TFile(nameF.Data(), "RECREATE", " Histograms", 1);
  gr_rootFile->cd();
  if (gr_rootFile->IsZombie()){
    cout<<"  ERROR ---> file "<<nameF.Data()<<" is zombi"<<endl;
    assert(0);
  }
  else
    cout<<"  Output Histos file ---> "<<nameF.Data()<<endl;
  for(unsigned int i = 0;i<v_gr.size();i++){
    if(i%1 == 0){
      v_gr.at(i)->Write();
    }
  }
  //

  gr_rootFile->Close();
  
  ///////////////
  /*
  for(unsigned int i = 0;i<v_gr_cut.size();i++){
    if(i == 0){
      for( Int_t j = 0; j < v_gr_cut.at(0)->GetN(); j++){
	v_gr_cut.at(0)->GetPoint(j,t_ave,a);
	gr_ave->SetPoint(j,t_ave,a);
      }
    }
    else{
      for( Int_t j = 0; j < v_gr_cut.at(0)->GetN(); j++){
	gr_ave->GetPoint(j,t_ave,a_ave);
	a = v_gr_cut.at(i)->Eval(t_ave);
	gr_ave->SetPoint( j, t_ave, (a + a_ave));
      }
    }
    if(i==5)
      shift_and_norm_gr(v_gr_cut.at(i),gr_rnd_norm,true); 
    //
    //Double_t t, t_ave, a, a_ave;    
    //gr_ave->
    //v_gr_cut.at(i);
    //}
  }
  //
  for( Int_t j = 0; j < v_gr_cut.at(0)->GetN(); j++){
    gr_ave->GetPoint(j,t_ave,a_ave);
    a_ave = a_ave/v_gr_cut.size();
    gr_ave->SetPoint( j, t_ave, a_ave);
  }
  ///////////////
  //get_baseline_info( gr_ave, load_R, baseline_window_in_ns, baseline_ampl, baseline_charge);
  shift_and_norm_gr(gr_ave,gr_ave_norm,true);
  shift_and_norm_gr(gr_ave,gr_ave_shift,false);
  //
  TString wf_csv_file_name;
  wf_csv_file_name = txtFile; wf_csv_file_name += "_norm.dat"; 
  save_gr_file( gr_ave_norm, wf_csv_file_name);
  wf_csv_file_name = txtFile; wf_csv_file_name += "_shift.dat"; 
  save_gr_file( gr_ave_shift, wf_csv_file_name);
  ///////////////
  TString nameF = rootFile;
  nameF += "_gr.root";
  TFile* gr_rootFile = new TFile(nameF.Data(), "RECREATE", " Histograms", 1);
  gr_rootFile->cd();
  if (gr_rootFile->IsZombie()){
    cout<<"  ERROR ---> file "<<nameF.Data()<<" is zombi"<<endl;
    assert(0);
  }
  else
    cout<<"  Output Histos file ---> "<<nameF.Data()<<endl;
  for(unsigned int i = 0;i<v_gr.size();i++){
    if(i%1 == 0){
      v_gr.at(i)->Write();
    }
  }
  //
  h1_base_line_ampl->Write();
  h1_base_line_ampl_cut->Write();
  h1_tot_charge->Write();
  h1_tot_charge_cut->Write();
  gr_ave->Write();
  gr_ave_norm->Write();
  gr_ave_shift->Write();
  gr_rnd_norm->Write();
  //
  gr_rootFile->Close();
  */
  ///////////////
}

Float_t get_total_charge(TGraph *gr, Float_t R){
  Float_t total_charge = 0.0;
  if(gr->GetN()>1){
    Double_t t,a;
    gr->GetPoint(0,t,a);
    Double_t t_start = t;
    gr->GetPoint((gr->GetN()-1),t,a);
    Double_t t_stop = t;
    Double_t dt = (t_stop - t_start)/(gr->GetN()-1);
    //
    for(Int_t i = 0;i<gr->GetN();i++){
      gr->GetPoint(i,t,a);
      total_charge += a;
    }    
    //
    if(R>0){
      return total_charge*dt/R;
    }
    else{
      assert(0);
    }
    //
  }
  return 0.0;
}

void get_baseline_info( TGraph *gr, Float_t R, Float_t baseline_window_in_ns, Float_t &baseline_ampl, Float_t &baseline_charge){
  baseline_ampl = 0.0;
  baseline_charge = 0.0;
  Double_t t0, t, a;
  Int_t nn = 0;
  if(gr->GetN()>0){
    gr->GetPoint(0,t0,a);
    for(Int_t i = 0;i<gr->GetN();i++){
      gr->GetPoint(i,t,a);
      if((t - t0)/1.0e-9>baseline_window_in_ns){
	nn=i;
	break;
      }
    }
    gr->GetPoint(0,t,a);
    Double_t t_start = t;
    gr->GetPoint(nn,t,a);
    Double_t t_stop = t;
    Double_t dt = (t_stop - t_start)/nn;
    //
    for(Int_t i = 0;i<nn;i++){
      gr->GetPoint(i,t,a);
      baseline_ampl += a;
      baseline_charge += a;
    }    
    //
    baseline_ampl /= nn;
    if(R>0)
      baseline_charge = baseline_charge*dt/R;
    else
      assert(0);
  }
}

Float_t get_max_ampl( TGraph *gr, Float_t &max_ampl_time){
  Double_t t_max_a, max_a, t, a;
  t_max_a = 0.0;
  max_a = -999;
  for(Int_t i = 0;i<gr->GetN();i++){
    gr->GetPoint(i,t,a);
    if(max_a<a){
      t_max_a = t;
      max_a = a;
    }
  }
  max_ampl_time = t_max_a;
  return max_a;
}

void shift_and_norm_gr( TGraph *gr, TGraph *gr_norm, Bool_t if_norm_max){
  Float_t baseline_ampl, baseline_charge;
  Float_t max_ampl, max_ampl_time;
  Double_t t_new, a_new, t, a;
  get_baseline_info( gr, load_R, baseline_window_in_ns, baseline_ampl, baseline_charge);
  //cout<<"baseline_ampl "<<baseline_ampl<<endl;
  max_ampl=get_max_ampl( gr, max_ampl_time);
  for(Int_t i = 0;i<gr->GetN();i++){
    gr->GetPoint(i,t,a);
    t_new = t;
    if(if_norm_max)
      a_new = (a-baseline_ampl)/(max_ampl - baseline_ampl);
    else
      a_new = (a-baseline_ampl);
    gr_norm->SetPoint(i,t_new,a_new);
  }
}

void save_gr_file( TGraph *gr, TString file_name){
  Double_t t, a;
  ofstream file_out;
  file_out.open(file_name.Data());
  for(Int_t i = 0;i<gr->GetN();i++){
    gr->GetPoint(i,t,a);
    file_out<<setw(20)<<t<<setw(20)<<a<<endl;
  }
  file_out.close();
}
