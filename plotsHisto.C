TH1D* readHisto(TString name,TString nameH);

Int_t plotsHisto(){
  TH1D *h1_01 = readHisto("../data/03.11.2023/F1--trace--00000.txt","../data/03.11.2023/F1--trace--00000.txt");
  TH1D *h1_02 = readHisto("../data/03.11.2023/F1--trace--00001.txt","../data/03.11.2023/F1--trace--00001.txt");
  TH1D *h1_03 = readHisto("../data/03.11.2023/F1--trace--00002.txt","../data/03.11.2023/F1--trace--00002.txt");
  TH1D *h1_04 = readHisto("../data/03.11.2023/F1--trace--00003.txt","../data/03.11.2023/F1--trace--00003.txt");
  //
  h1_01->Draw();
  //
  return 0;
}

TH1D* readHisto(TString name,TString nameH){
  const Int_t nnnMax = 10000;
  Double_t xx[nnnMax];
  Double_t yy[nnnMax];
  Double_t xxx;
  Double_t yyy;
  string mot;
  ifstream myfile (name.Data());
  assert(myfile.is_open());
  //Time    Ampl
  Int_t nBins;
  myfile>>mot>>mot>>mot;  //LECROYWP740Zi   49284   Histogram
  myfile>>mot>>mot>>mot>>nBins;  //Segments        1       SegmentSize     455
  cout<<"nBins = "<<nBins<<endl;
  myfile>>mot>>mot>>mot;  //Segment TrigTime        TimeSinceSegment1
  myfile>>mot>>mot>>mot>>mot;  //#1      29-Nov-2012 16:14:45    0
  cout<<mot<<endl;
  myfile>>mot>>mot;
  cout<<mot<<endl;
  Int_t i = 0;
  for(i = 0;i<nBins;i++){
    myfile>>xxx>>yyy;
    xx[i] = xxx;
    yy[i] = yyy;
    //cout<<"xxx = "<<xxx<<endl;
  }
  myfile.close();
  Double_t binWidth = (xx[nBins-1] - xx[0])/(nBins-1);
  Double_t xMin = xx[0] - binWidth/2.0;
  Double_t xMax = xx[nBins-1] + binWidth/2.0;
  cout<<"xMin = "<<xMin<<endl
      <<"xMax = "<<xMax<<endl
      <<"binWidth = "<<binWidth<<endl;
  TString hhN = "hh1_";
  hhN += nameH;
  TH1D *hh1 = new TH1D(hhN.Data(),"hh1",nBins,xMin,xMax);  
  for(i = 0;i<nBins;i++){
    //cout<<(hh1->GetBinCenter(i+1) - xx[i])<<endl;
    hh1->SetBinContent(i+1,yy[i]);
  }
  return hh1;
}
