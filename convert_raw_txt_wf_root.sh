#!/bin/bash

function printHelp {
    echo " --> ERROR in input arguments "
    echo " [0] --03.11.2023 : convert data"
    echo " [0] -h           : print help"
}

if [ $# -eq 0 ] 
then    
    printHelp
else
    if [ "$1" = "--03.11.2023" ]; then
	filein="../data/03.11.2023/C1--trace--00000.txt"
	out_log="../data/03.11.2023/C1--trace--00000.log"
	rm -rf $out_log
	./convert_raw_txt_wf_root 0 $filein -2.0034e-10 -1.9507e-10 -0.01490 -0.0135 | tee -a $out_log
	filein="../data/03.11.2023/C1--trace--00001.txt"
	out_log="../data/03.11.2023/C1--trace--00001.log"
	rm -rf $out_log
	./convert_raw_txt_wf_root 0 $filein -2.0034e-10 -1.9507e-10 -0.01490 -0.0135 | tee -a $out_log
    elif [ "$1" = "-h" ]; then
        printHelp
    else
        printHelp
    fi
fi

#espeak "I have done"
